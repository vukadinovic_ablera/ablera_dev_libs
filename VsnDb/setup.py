import setuptools

setuptools.setup(
    name="vsndb",
    version="0.0.1",
    author="Milos Vukadinovic",
    author_email="milos.vukadinovic@ablera.com",
    description="Package providing connection to vision database ( images and models )",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
