import cx_Oracle
import logging

class DbConnection:
    """ Interact with srd_vsn databse """

    def __init__(self, config):
        dsn_tns = cx_Oracle.makedsn(config.DSN, config.PORT, config.SID)
        self.conn = cx_Oracle.connect(user=config.USERNAME, password=config.PASSWORD, dsn=dsn_tns)
        self.query = self.conn.cursor()
        self.logger = logging.getLogger(__name__)

    #helper func def list_models(self):
    def list_models(self):
        """ Display all models in db"""

        self.query.execute('select * from sr_vsn_model_custom_architectures') 
        print("ID - ", "Name - ", "Version - ", "Storage")
        for row in self.query:
            print (row[0]-1, '-', row[2], '-', row[4] , '-' ,row[10])

    def get_model_by_id(self, model_id):
        """ Get model's path """

        stmt = """ select arch.model_storage, arch.model_weights_path, arch.model_graph_image_path 
        FROM sr_vsn_models models 
        INNER JOIN sr_vsn_model_custom_architectures arch
        ON models.sr_vsn_model_custom_architecture_id = arch.sr_vsn_model_custom_architecture_id
        where models.sr_vsn_model_id =""" +str(model_id)

        self.query.execute(stmt)
        result =  self.query.fetchone()
        return (result[0], result[1], result[2])

    def create_images(self, img_paths):

        try:
            stmt = "insert all "
            for pth in img_paths:
                stmt = stmt+"into sr_vsn_images (image_path) VALUES (\'"+pth+"\') "
            stmt = stmt + " SELECT 1 from DUAL"
            self.query.execute(stmt) 
            self.conn.commit()
            stmt = "SELECT sr_vsn_image_id FROM sr_vsn_images WHERE ("
            for i in range(len(img_paths)):
                if i!= len(img_paths)-1:
                    stmt=stmt+"image_path=\'"+str(img_paths[i])+ "\' OR "
                else:
                    stmt=stmt+"image_path=\'"+str(img_paths[i])+ "\')" 
            self.query.execute(stmt)
            image_ids = []
            for row in self.query:
                image_ids.append(row[0])
            return image_ids

        except Exception as e:
            self.logger.error("ERROR OCCURED on command "+stmt)
            self.logger.error(e)
    
    def log_prediction(self, image_id, column_name, prob_damage, current_time):
        try:
            stmt = "UPDATE sr_vsn_images SET "+ str(column_name) + " = " + str(prob_damage) + " , date_processed = TO_TIMESTAMP(\'" + current_time + "\',\'RRRR-MM-DD HH24:MI:SS\'"+")" + " WHERE sr_vsn_image_id = " + str(image_id)

            self.query.execute(stmt) 
            self.conn.commit()
        except Exception as e:
            self.logger.error("ERROR OCCURED on command "+stmt)
            self.logger.error(stmt)


    def get_image_paths(self, image_ids):
        try:
            stmt = "SELECT IMAGE_PATH FROM SR_VSN_IMAGES WHERE ("
            for i in range(len(image_ids)):
                if i != len(image_ids)-1:
                    stmt=stmt+"sr_vsn_image_id="+str(image_ids[i])+" OR "
                else:
                    stmt=stmt+"sr_vsn_image_id="+str(image_ids[i])+")"

            self.query.execute(stmt)
            image_paths = []
            for row in self.query:
                image_paths.append(row[0])

            return image_paths
        except Exception as e:
            print(e)
            print(stmt)

    #def set_labels(image_id,front_bumper_dmg,front_bumper_severity,back_bumper_dmg):

    def __del__(self): 
        self.conn.close()
