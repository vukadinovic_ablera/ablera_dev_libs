import io 
import base64
import os
import importlib.util
from PIL import Image
import torchvision.transforms as transforms
import torch
import torch.nn as nn
from vsndb import DbConnection
from storage import AzureStorage, LocalStorage

class Model:
    """ Interact with srd_vsn databse"""
    __instance = None

    @staticmethod
    def getInstance(logger, config):
        """ Singleton """ 
        if Model.__instance == None:
            Model.__instance = Model(logger, config)
        return Model.__instance

    def __init__(self, logger, config):
        if Model.__instance == None:
            self.con = DbConnection(config)
            self.config = config
            self.con.list_models()
            self.logger = logger
            self.model = None
            self.logger.info("Starting Damage Classifier")

    def load_model(self, model_id):
        """Accesses storage and loads a model"""

        storage_name, weights_path, conf_path = self.con.get_model_by_id(model_id)
        self.logger.info("Loading model "+ weights_path+ " from "+storage_name)

        if storage_name == "azure":
            storage = AzureStorage(self.config)
        elif storage_name == "local":
            storage = LocalStorage(self.config)

        obj = None
        weights_byte, conf_byte = storage.fetch_model(weights_path)
        obj = self.__import_from_bytes(conf_byte, conf_path)

        #If model needs conf file, load it
        if obj == None:
            self.model = torch.load(io.BytesIO(weights_byte),map_location=torch.device('cpu'))
        elif obj != None:
            self.model = obj
            self.model.model.load_state_dict(torch.load(io.BytesIO(weights_byte),map_location=torch.device('cpu')))
            self.model.eval()

    def serve_model(self, image_ids, storage_name):
        """ Takes loaded model and performs evaluation, returns it to the user and saves it to the storage """

        if storage_name == 'azure':
            storage = AzureStorage(self.config) 
        elif storage_name == 'local':
            storage = LocalStorage(self.config)

        image_paths =  self.con.get_image_paths(image_ids)
        image_bytes_batch = storage.get_image_bytes_batch(image_paths) 
        result = []
        """ In case you want to run on GPU """
        #device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        device = torch.device("cpu")
        image_tensors = [self.__transform_image(image_bytes=image_bytes) for image_bytes in image_bytes_batch]
        tensor = torch.cat(image_tensors).to(device)
        outputs = self.model.forward(tensor)

        #some models return tuple 
        if(type(outputs) is tuple):
            output=outputs[0]
            states=output[1]
        else: 
            output = outputs

        try:
            output = nn.functional.softmax(output)
            prob, y_hat = torch.max(output, 1)
            predicted_ids = y_hat.tolist()
            probabilities = prob.tolist()
        except Exception as e:
            self.logger.error(e)

        for i in range(len(image_tensors)):
            result.append({"image_id": image_ids[i],
                            "dmg": (y_hat[i] == 0).numpy().item(0),
                            "probability" : probabilities[i]})

        # TODO save label in database, you have image_ids and probabilities
        return result

    def __transform_image(self, image_bytes):
        """ private fcn to process the image """
        my_transforms = transforms.Compose([transforms.Resize((896, 896)),
                                            transforms.ToTensor(),
                                            transforms.Normalize([0.485, 0.456, 0.406],
                                                                 [0.229, 0.224, 0.225])])
        image = Image.open(io.BytesIO(image_bytes))
        return my_transforms(image).unsqueeze(0)

    def __import_from_bytes(self, file_bytes, class_name):
        try:
            #write to file
            with open('conf.py', 'wb') as f:
                data = file_bytes
                f.write(data) 
            #import file
            spec = importlib.util.spec_from_file_location(class_name, 'conf.py')
            lib = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(lib)
            #instantiate object
            obj = getattr(lib, class_name)() 
            #remove conf file
            os.remove('conf.py')
        except:
            obj = None
        return obj
