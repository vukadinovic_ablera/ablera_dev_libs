import setuptools

setuptools.setup(
    name="storage",
    version="0.0.1",
    author="Milos Vukadinovic",
    author_email="milos.vukadinovic@ablera.com",
    description="Package for accessing storages (azure, local...) and save/get models and images",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
