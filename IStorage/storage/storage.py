import abc
import base64
import uuid
from azure.storage.filedatalake import DataLakeServiceClient
import paramiko
from scp import SCPClient

class IStorage(abc.ABC):
    """ Interface for Storage"""
    @abc.abstractmethod
    def fetch_model(self, file_name):
        pass
    def upload_model(self, file_path):
        pass
    def upload_image(self, image_name, image_content):
        pass
    def get_image_bytes_batch(self, img_paths):
        pass

@IStorage.register
class AzureStorage:
    """Azure data lake storage"""

    def __init__(self,config):
        """ Create a connection """
        try: 
            self.config = config
            self.service_client = DataLakeServiceClient(account_url=
            "{}://{}.dfs.core.windows.net".format("https", self.config.STORAGE_ACCOUNT_NAME),
            credential=self.config.STORAGE_ACCOUNT_KEY)
        except Exception as e:
            print(e)

    def fetch_model(self,file_name):
        """ Downloads files needed to load the model"""
        try:

            file_system_client = self.service_client.get_file_system_client(file_system=self.config.FILE_SYSTEM)

            directory_client = file_system_client.get_directory_client(file_name)

            weights_file_client = directory_client.get_file_client("weights.pth")
            download_weights = weights_file_client.download_file()
            weights = download_weights.readall()
            try:
                conf_file_client = directory_client.get_file_client("conf.py")
                download_conf = conf_file_client.download_file()
                conf = download_conf.readall()
                return (weights, conf)
            except:
                conf=None

            return (weights,conf)
        except Exception as e:
            print(e)

    def upload_model(self, directory_name):
        """ Uploads specified pth file to the storage"""
        try:

            file_system_client = self.service_client.get_file_system_client(file_system=self.config.FILE_SYSTEM)
            file_system_client.create_directory(directory_name)
            
            directory_client = file_system_client.get_directory_client(directory_name)

            weights = directory_client.get_file_client("weights.pth")
            with open(directory_name+"/weights.pth", "rb") as f:
                weights_byte = f.read()
            weights.upload_data(weights_byte, overwrite=True)

            try:
                conf = directory_client.get_file_client( "conf.py")
                with open(directory_name+"/conf.py", "rb") as f:
                    conf_bytes = f.read()
                conf.upload_data(conf_bytes, overwrite=True)
            except:
                print("No conf file, only weights uploaded") 

        except Exception as e:
            print(e) 

    def upload_images(self, images64):    
        try:    
            
            file_system_client = self.service_client.get_file_system_client(file_system=self.config.IMAGE_FILE_SYSTEM)    
            directory_client = file_system_client.get_directory_client("image_dir")    
            img_paths = []
            for img in images64: 
                img_data = base64.b64decode(img)
                img_name = uuid.uuid4().hex+".jpg"
                file_client = directory_client.get_file_client(img_name)    
                img_paths.append(img_name)
                file_client.upload_data(img_data, overwrite=True) 
            return img_paths
        except Exception as e:    
            print(e)  

    def get_image_bytes_batch(self,img_paths):
        try:
            file_system_client = self.service_client.get_file_system_client(file_system=self.config.IMAGE_FILE_SYSTEM)    
            directory_client = file_system_client.get_directory_client("image_dir")    
            image_bytes_batch = []
            for pth in img_paths:
                file_client = directory_client.get_file_client(pth)
                download = file_client.download_file()
                downloaded_bytes = download.readall()
                image_bytes_batch.append(downloaded_bytes)
            return image_bytes_batch
        except Exception as e:
            print(e)


@IStorage.register
class LocalStorage:

    def __init__(self,config):
        """ Create a connection """
        try: 
            self.config = config
        except Exception as e:
            print(e)

    def fetch_model(self, file_name):
        """Return bytes for files needed to load the model"""
        try:
            with open("/storage/model_repository/"+file_name+"/conf.py", 'rb') as f:
                conf = f.read()
        except:
            conf = None
        try:
            with open("/storage/model_repository/"+file_name+"/weights.pth", 'rb') as f:
                weights = f.read()
                return (weights, conf)
        except Exception as e:
            print(e)

    def upload_model(self, dir_name):
       client = paramiko.SSHClient()
       client.load_system_host_keys()
       client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
       client.connect(self.config.DSN, 22, "sshfs", self.config.PASSWORD)
       ssh = client
       scp = SCPClient(ssh.get_transport())
       scp.put(dir_name, recursive=True, remote_path='/datastore/model_repository/')
       scp.close()

    def upload_images(self, images64):
        img_paths = []
        for img in images64:
            img_data = base64.b64decode(img)
            img_name = "/storage/images/"+uuid.uuid4().hex +".jpg"
            img_paths.append(img_name)
            with open(img_name, 'wb') as f:
                f.write(img_data) 
        return img_paths

    def get_image_bytes_batch(self,img_paths):
        image_bytes_batch = []
        try:
            for img_pth in img_paths:
                with open(img_pth,'rb') as f:
                    img = f.read()
                    image_bytes_batch.append(img)

            return image_bytes_batch
        except Exception as e:
            print(e)